//1.Importamos NgMOdule para indicarle que la clase contenida en este archivo es un módulo
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PorPaisComponent } from './pais/pages/por-pais/por-pais.component';
import { PorCapitalComponent } from './pais/pages/por-capital/por-capital.component';
import { VerPaisComponent } from './pais/pages/ver-pais/ver-pais.component';
import { PorRegionComponent } from './pais/pages/por-region/por-region.component';

//4. Definimos el arreglo que contendra la definición de las rutas
const routes: Routes = [
  {
    path: '',
    component: PorPaisComponent,
    pathMatch: 'full',
  },
  {
    path: 'region',
    component: PorRegionComponent,
  },
  {
    path: 'capital',
    component: PorCapitalComponent,
  },
  {
    path: 'pais/:id',
    component: VerPaisComponent,
  },
  {
    //5. Definimos la ruta a la que se redigira al usuario en caso de colocar una que no exista en el arreglo
    path: '**',
    redirectTo: '',
  },
];

//3. colocamos su definición con su metadata
@NgModule({
  imports: [
    //6. Llamar RouterModule para que se configuren las rutas definidas arriba
    RouterModule.forRoot(routes), //forRoot indica las rutas principales
  ],
  exports: [
    //7. Se exporta el modulo para que este disponible en la app
    RouterModule,
  ],
})

//2. creamos la clase del modulo
export class AppRoutingModule {}

//8. Hay que importar este modulo desde AppModule y ocupar <router-outlet> en el html
