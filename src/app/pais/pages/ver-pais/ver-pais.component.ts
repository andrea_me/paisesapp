import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';

import { Country, Language } from './../../interfaces/pais.interface';

import { PaisService } from './../../services/pais.service';

@Component({
  selector: 'app-ver-pais',
  templateUrl: './ver-pais.component.html',
  styleUrls: ['./ver-pais.component.scss'],
})
export class VerPaisComponent implements OnInit {
  /*! -> evita que Ts informe que esta propiedad es nula o indefinida,
  en este caso puede que ocurra ya que no esta inicilizada */
  pais!: Country;
  languages: Language[] = [];

  /**
   * ActivatedRoute, tiene métodos que ayudan a suscribirse a cualquier cambio dentro de una url
   */
  constructor(
    private activatedRoute: ActivatedRoute,
    private paisService: PaisService
  ) {}

  ngOnInit() {
    //Se tiene acceso al observable en donde estan los parametros que se pasan por url
    this.activatedRoute.params
      //a través de un pipe ejecutamos un switchMap
      .pipe(
        //switchMap, recibe el observable de params y retorna el observable que manda el servicio pais
        switchMap(({ id }) => this.paisService.buscarPorIdPais(id))
        //tap, recibe el producto del observable anterior e imprime en consola ese resultado
        //tap(console.log)
      )
      .subscribe((pais) => (this.pais = pais));
  }
}
