import { Component } from '@angular/core';
import { Country } from '../../interfaces/pais.interface';
import { PaisService } from './../../services/pais.service';

@Component({
  selector: 'app-por-region',
  templateUrl: './por-region.component.html',
  styles: [],
})
export class PorRegionComponent {
  regiones: string[] = ['africa', 'americas', 'asia', 'europe', 'oceania'];

  //almacena la region que fue seleccionada por el usuario
  regionActiva: string = '';
  existError: boolean = false;
  paises: Country[] = [];

  constructor(private paisService: PaisService) {}

  getClaseCSS(region: string): string {
    return region === this.regionActiva ? 'btn-primary' : 'btn-outline-primary';
  }

  activarRegion(region: string) {
    //si se selecciona consecutivamente la misma región, no cargues de nuevo la info
    if (region === this.regionActiva) {
      return;
    }

    this.regionActiva = region;
    this.paises = []; //borra los paises, antes de realizar una nueva consulta

    this.paisService
      .buscarPorRegion(region)
      .subscribe((paises) => (this.paises = paises));
  }
}
