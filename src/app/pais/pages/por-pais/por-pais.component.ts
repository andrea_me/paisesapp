import { Component } from '@angular/core';

import { Country } from './../../interfaces/pais.interface';

import { PaisService } from './../../services/pais.service';

@Component({
  selector: 'app-por-pais',
  templateUrl: './por-pais.component.html',
  styles: [
    `
      li {
        cursor: pointer;
      }
    `,
  ],
})
export class PorPaisComponent {
  termino: string = '';
  existError: boolean = false;
  paises: Country[] = [];

  paisesSugeridos: Country[] = [];
  mostrarSugerencias: boolean = false;

  constructor(private paisService: PaisService) {}

  buscar(termino: string) {
    this.existError = false;
    this.mostrarSugerencias = false;

    //se asigna a la propiedad de la clase el termino que envia el componente hijo(pais-input)
    this.termino = termino;

    this.paisService.buscarPais(this.termino).subscribe(
      (paises) => {
        //console.log(paises);
        this.paises = paises;
      },
      (err) => {
        this.existError = true;
        this.paises = [];
      }
    );
  }

  sugerencias(termino: string) {
    this.existError = false;
    this.termino = termino;
    this.mostrarSugerencias = true;

    this.paisService.buscarPais(termino).subscribe(
      //dependiendo del termino, trae solo 4 paises para mostrarlos como sugerencia
      (paises) => (this.paisesSugeridos = paises.splice(0, 5)),
      //si ocurre un error, no muestres nada
      (err) => (this.paisesSugeridos = [])
    );
  }
}
