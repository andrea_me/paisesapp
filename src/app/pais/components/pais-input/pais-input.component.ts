import { Component, Output, EventEmitter, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-pais-input',
  templateUrl: './pais-input.component.html',
  styles: [],
})
export class PaisInputComponent implements OnInit {
  termino: string = '';

  @Input() placeholder: string = '';

  //Se coloca una propiedad que será la encargada de emitir el termino al elemento padre
  @Output() sendInput: EventEmitter<string> = new EventEmitter();

  //Se coloca otro evento de salida cuando el usuario deja de escribir
  @Output() eventDebounce = new EventEmitter<string>();

  /** Subject es un tipo de observable*/
  debouncer: Subject<string> = new Subject();

  constructor() {}

  ngOnInit(): void {
    this.debouncer.pipe(debounceTime(300)).subscribe((valor) => {
      this.eventDebounce.emit(valor);
    });
  }

  buscar() {
    this.sendInput.emit(this.termino);
  }

  teclaPresionada() {
    this.debouncer.next(this.termino);
  }
}
