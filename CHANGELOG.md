# Changelog

All notable changes to this project will be documented in this file. See
[standard-version](https://github.com/conventional-changelog/standard-version)
for commit guidelines.

## [1.1.0](https://gitlab.com/andrea_me/paisesapp/-/compare/v1.0.0...v1.1.0) (2021-08-18)

### Features

- add loader functionality in por-region.component-html
  ([8695186](https://gitlab.com/andrea_me/paisesapp/-/commit//86951868178472b0f86a7a2bb72c99372ed6d6c0))
- add meta tags in index.html
  ([92795ab](https://gitlab.com/andrea_me/paisesapp/-/commit//92795abc61025332b08fb06d1e66e0e339d41cb5))
- create an interceptor for spinner
  ([cf78f0b](https://gitlab.com/andrea_me/paisesapp/-/commit//cf78f0b43388fa736f1c0804f75099f007c7605f))
- create service for spinner
  ([ba3f260](https://gitlab.com/andrea_me/paisesapp/-/commit//ba3f260d78a9045f631234a547462b207225832a))
- create spinner component and exports from sharedModule
  ([a0fd284](https://gitlab.com/andrea_me/paisesapp/-/commit//a0fd28416da4e80efc28f092128ec9af48734666))

### Bug Fixes

- add meta tags for shared in social media
  ([cb56491](https://gitlab.com/andrea_me/paisesapp/-/commit//cb5649173053071bd6e080bca9f32241ea8d6489))
- modify app name
  ([ee48d55](https://gitlab.com/andrea_me/paisesapp/-/commit//ee48d55d72325680e6d15358db20b0c45c665656))
- **ver-pais:** add superindex in area text
  ([1fab97d](https://gitlab.com/andrea_me/paisesapp/-/commit//1fab97d7aa67a8aeaf27aa045ce7661952de2db4))

### Style

- **global:** modify the button's shadow color
  ([63c4c67](https://gitlab.com/andrea_me/paisesapp/-/commit//63c4c6756c24fa4d75025ee60bc578732c3a64ad))

### Chore

- fix project name in angular.json
  ([a00d45d](https://gitlab.com/andrea_me/paisesapp/-/commit//a00d45d4da2cac64550b654229d847d3c1cedc5b))
- modify readme file
  ([9e5f735](https://gitlab.com/andrea_me/paisesapp/-/commit//9e5f735c39ef41c8857e3dc7295974f2bfc73f5c))
- modify title readme file
  ([d834255](https://gitlab.com/andrea_me/paisesapp/-/commit//d834255b4d6288aedaf3e6098458624f4ebdca09))

## [1.0.0](https://gitlab.com/andrea_me/paisesapp/-/compare/v0.0.1...v1.0.0) (2021-08-16)

### Features

- add and import new styles file for variables
  ([2440bb3](https://gitlab.com/andrea_me/paisesapp/-/commit//2440bb31f689b8fc08916a5fd162e8fb31b225db))
- add animation in pages and table component
  ([9871eeb](https://gitlab.com/andrea_me/paisesapp/-/commit//9871eeb9440731d26e41da1379eb5ab9a13746f6))
- add buscarCapital method in pais service
  ([1fa8d00](https://gitlab.com/andrea_me/paisesapp/-/commit//1fa8d0084e073fef6f51bbdaff04955bfd3474f5))
- add country search by id method in pais.service.ts
  ([85d515b](https://gitlab.com/andrea_me/paisesapp/-/commit//85d515b834464995b76c1f47c3d866de7b1c3fde))
- add debounceTime in search input to update its status when the users stops
  typing
  ([170efac](https://gitlab.com/andrea_me/paisesapp/-/commit//170efac207b59b253a432a80cd1ca47c3d1d6cc7))
- add footer in app component and modify styles
  ([7173990](https://gitlab.com/andrea_me/paisesapp/-/commit//717399090b566f05d1c9858284c728bc218541b7))
- add functionality for capital search
  ([0341df1](https://gitlab.com/andrea_me/paisesapp/-/commit//0341df1f8a2886dc58b52ffa2d126f7467cd83aa))
- add functionality for region search
  ([bce85cd](https://gitlab.com/andrea_me/paisesapp/-/commit//bce85cd9358753a9c09a7e80bdbfdabe5b3f2129))
- add global styles for buttons
  ([bbac5eb](https://gitlab.com/andrea_me/paisesapp/-/commit//bbac5eb1a7399cfe4e6ee4a8ff19e802a57ecf0f))
- add logo to app
  ([e380450](https://gitlab.com/andrea_me/paisesapp/-/commit//e380450891ae910de0118f0cbe15085bc09f0645))
- add search method by region in pais.service.ts
  ([bd9dbb1](https://gitlab.com/andrea_me/paisesapp/-/commit//bd9dbb182c477d36784f29d4ad31d6eed6b549b0))
- add template and styles to verPaisComponent
  ([ef5751a](https://gitlab.com/andrea_me/paisesapp/-/commit//ef5751abbc194ae4d8d5f880e462409537d8b09c))
- create component for table display only
  ([709a793](https://gitlab.com/andrea_me/paisesapp/-/commit//709a7937dfff0ffad265262c94ec89df74e29fe1))
- create interface for country search
  ([015be0b](https://gitlab.com/andrea_me/paisesapp/-/commit//015be0b642bde5851f0b7d18bd2d16586f85102c))
- create modules and basic components
  ([834e4cb](https://gitlab.com/andrea_me/paisesapp/-/commit//834e4cbe7481e144e93c00c1cf294a17acfc6b7b))
- create PaisInputComponent to communicate the search to its parent component
  using @Output
  ([3c54680](https://gitlab.com/andrea_me/paisesapp/-/commit//3c54680aa67e7d618585d75d21980c83f4ed1df4))
- create RouterModule and use it in sideBarComponent
  ([1b03883](https://gitlab.com/andrea_me/paisesapp/-/commit//1b03883b0e05ab5014426e92d3b439f712e8008e))
- create stream for country search and display results
  ([2584d27](https://gitlab.com/andrea_me/paisesapp/-/commit//2584d279fb04af375c507c79568f2c4d4d724315))
- edit paisComponent with basic html structure
  ([03f9ff7](https://gitlab.com/andrea_me/paisesapp/-/commit//03f9ff77b2168b3847be08b9ee53bbf2489c8b6f))
- get results of the country search by id
  ([b555b7e](https://gitlab.com/andrea_me/paisesapp/-/commit//b555b7ebf495c0e8e026b31bb7b3a27ad1498223))
- get results of the country search by id using switchMap
  ([e9f1dcb](https://gitlab.com/andrea_me/paisesapp/-/commit//e9f1dcbe3b58f6649fdea95c49016d91adc3ce13))
- **index:** add the animate-css CDN in index.html
  ([745bdea](https://gitlab.com/andrea_me/paisesapp/-/commit//745bdead76e81840f5109a1686dfc185e595f932))
- **pais-input:** add placeholder property for manage dinamically
  ([41c4074](https://gitlab.com/andrea_me/paisesapp/-/commit//41c407441625b9ffde002115c6d366d2a7703e73))
- **por-capital:** create autocomplete for capital search
  ([4d7ce48](https://gitlab.com/andrea_me/paisesapp/-/commit//4d7ce486068d87357ec8de06ee194de4e24473f6))
- **por-pais:** create autocomplete for country search
  ([910c873](https://gitlab.com/andrea_me/paisesapp/-/commit//910c873e742e512a70bbb009fc2b7f466e5abecf))
- update favicon.ico in project
  ([41c48ed](https://gitlab.com/andrea_me/paisesapp/-/commit//41c48ed7e0a35eb8c227b474a4689f0585c94762))

### Bug Fixes

- add and modify custom styles for sidebarComponent
  ([cc45ce4](https://gitlab.com/andrea_me/paisesapp/-/commit//cc45ce4bde165f988316dcce9b3121bce9ae3ee1))
- add container external in appComponent
  ([a9de76e](https://gitlab.com/andrea_me/paisesapp/-/commit//a9de76eeba18d7046680a0447148a67626f00df7))
- add header with logo in appComponent
  ([fadeb15](https://gitlab.com/andrea_me/paisesapp/-/commit//fadeb158c841cee6aaa8f28fb8475414bcd14e16))
- add placeholder attribute in por-pais-component.html
  ([e27e71b](https://gitlab.com/andrea_me/paisesapp/-/commit//e27e71bafa1d8b8ee8a63c6c90d6ecc0884280fd))
- modify country and capital name text
  ([56d193a](https://gitlab.com/andrea_me/paisesapp/-/commit//56d193ac9b1792ff7fc442ada91694842f513d33))
- modify global styles
  ([4387451](https://gitlab.com/andrea_me/paisesapp/-/commit//438745149aec3c58c70be1adb7528bec04ff2e5a))
- modify styles and text to english in PorPaisCOmponent
  ([1910f1c](https://gitlab.com/andrea_me/paisesapp/-/commit//1910f1ce845f8d9cff530300566fa27d55abe5c3))
- modify styles and texts to english in PorCapitalComponent
  ([15725bd](https://gitlab.com/andrea_me/paisesapp/-/commit//15725bd5f381dcb50850ddba4be5d2f515c07c9c))
- modify styles and texts to english in PorRegionComponent
  ([ebf3118](https://gitlab.com/andrea_me/paisesapp/-/commit//ebf3118738be7fbeb07d85e007f869eba9490370))
- modify styles and texts to english in verPaisComponent
  ([1742777](https://gitlab.com/andrea_me/paisesapp/-/commit//1742777b07f3e547bbb4d7d347434e85c7ab54d1))
- modify text and style in pais-tabla.component.html
  ([a34e32b](https://gitlab.com/andrea_me/paisesapp/-/commit//a34e32b3b5eb0cc4f1ebdc84bf0edf57e3113936))
- modify text to english and add style in paisTablaComponent
  ([11a7d25](https://gitlab.com/andrea_me/paisesapp/-/commit//11a7d258a13c15574a28e3cb1847fb224e2dd706))
- optimize http searches in pais.service.ts
  ([60e6b5c](https://gitlab.com/andrea_me/paisesapp/-/commit//60e6b5c4229da2679dd9cbe95aac668fc19bb477))
- remove unnecessary folders
  ([c314ee3](https://gitlab.com/andrea_me/paisesapp/-/commit//c314ee30da043ba5f2f5040e3ca539d26028ede1))

### Chore

- add path to styles in angular.json
  ([fc92e5d](https://gitlab.com/andrea_me/paisesapp/-/commit//fc92e5d2f36d340e1e63feeaf969b755340c12b0))

### Refactor

- remove console.log method
  ([53f1cf1](https://gitlab.com/andrea_me/paisesapp/-/commit//53f1cf10511d212fe6ddb1134482433c622017c0))

### [0.0.1](https://gitlab.com/andrea_me/paisesapp/-/compare/v0.0.0...v0.0.1) (2021-07-19)

### Bug Fixes

- delete unnecessary huskyrc file
  ([51eaa67](https://gitlab.com/andrea_me/paisesapp/-/commit//51eaa6770c727043e5941a9175c10e8db66a3c12))

## 0.0.0 (2021-07-19)

### Chore

- create files to autogeneration CHANGELOG and validate commits with
  Conventional Commits
  ([44ef4b5](https://gitlab.com/andrea_me/paisesapp/-/commit//44ef4b5e7c1dc8401c7a46f7eeec7f056ea378c3))
- create hooks for pre-commit and commit-msg
  ([22aefee](https://gitlab.com/andrea_me/paisesapp/-/commit//22aefee72c0eb7f96093017554e4113fbc2d3c31))
- creates a folder structure for an Angular project
  ([4a56fa2](https://gitlab.com/andrea_me/paisesapp/-/commit//4a56fa2c24b96685d323d9b0c47dc9a0976c922e))
- creates a prettierrc file configuration
  ([a6729fe](https://gitlab.com/andrea_me/paisesapp/-/commit//a6729fe9770488a57c362efb514b0a0daf2b58d0))
- initial commit
  ([ab07a05](https://gitlab.com/andrea_me/paisesapp/-/commit//ab07a059ff5db6058ec6187543ad32893a7785eb))
- install and configure conventional-commits ans standard-version
  ([17f1a2c](https://gitlab.com/andrea_me/paisesapp/-/commit//17f1a2c2fcfb914b56dbfe91fbd72b81e800b589))
- install anxelin-project-structure package with npm
  ([999abb7](https://gitlab.com/andrea_me/paisesapp/-/commit//999abb7120458e1f8eb61feac33947a5a088a17b))
- install husky, prettier, prettier-quick and lint
  ([ba6f1ba](https://gitlab.com/andrea_me/paisesapp/-/commit//ba6f1ba5ac3235e00d7d1b04857fc92bf4e54195))
