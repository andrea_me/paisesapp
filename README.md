<div align="center" style="margin-bottom:30px">
    <img src="src/assets/img/logo.png" alt="logotipo">
    <p>This project contains an application that performs country searchs.</p>
    <a target="_blank" rel="noopener noreferrer" href="https://countries-app-great-site.netlify.app/">See website</a>      
</div>

> This project contains a hook that validate `lint` project and `prettier` rules
> every run the command `git commit -m '...'`

## Prerequisites

1. Node > 14.17.0
2. npm > 6.14.13
3. Install Angular CLI > 12.0.5

## Installation

1. Install dependencies

```shell
  npm ci
```

2. Start a dev server and opens in a browser

```shell
  ng serve -o
```

## Author

👤 **Andrea Morales**
